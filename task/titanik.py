import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df




def get_filled():
    df = get_titatic_dataframe()
    def get_prefix(name):
        name = name.split(',')[1]
        name = name.split('.')[0]
        return name.strip()
    
    df['Prefix'] = df['Name'].apply(get_prefix)'''
    df = df[df['Prefix'].isin(['Mr', 'Mrs', 'Miss'])]
    return None









# Apply the function to the 'Name' column and create a new column 'Prefix'


# Filter the dataframe by the prefixes of interest
df = df[df['Prefix'].isin(['Mr', 'Mrs', 'Miss'])]

# Group the dataframe by 'Prefix' and calculate the median of 'Age'
prefix_age = df.groupby('Prefix')['Age'].median().round()

# Fill the missing values in 'Age' with the median values based on 'Prefix'
df['Age'] = df['Age'].fillna(df['Prefix'].map(prefix_age))

# Count the number of missing values in 'Age' for each prefix
prefix_na = df[df['Age'].isna()].groupby('Prefix')['Age'].count()

# Create a list of tuples with the prefix, the number of missing values, and the median value
result = [(prefix, prefix_na.get(prefix, 0), prefix_age[prefix]) for prefix in ['Mr', 'Miss', 'Mrs']]

# Print the result
print(result)
